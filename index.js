// console.log('Hello, World');

// Javascript Synchronous vs Asynchronous


// Synchronous Javascript
// Javascript is by default synchronous, meaning that only one statement is being run/executed at a time.

/*console.log('Hello, World');
console.log('Hello, Again');
console.log('Goodbye');

for(let i = 0; i <= 10;i++) {
	console.log(i);
}

//blocking, is just a slow process of code.
console.log('Hello, World!');

// Asynchronous Javascript

function printMe() {
	console.log('print me');
}
setTimeout(printMe, 5000);

function test() {
	console.log('test');
}
test();*/

// The fetch API allows you to asynchronously request for a resource (date).
// The featch receives a PROMISE
// A 'PROMISE' in an objet that represents the eventual completion or failure of an synchronous function and its resulting value.

// fetch() is a method in JS, which allows us to send a request to an API and process its response
	
	// Syntax:
	// fetch(url, {options}).then(response => response.json()).then(data => {console.log()});

// url -> the url to resource/routes from the Server.
// optional objects -> it contains additional information about our requests such as method, the body and the headers.

// it parse the response as JSON
// process the results
console.log(fetch('https://jsonplaceholder.typicode.com/posts'));
	// A promise may be in one of 3 possible states: fulfilled, rejected or pending.



// Retreive all posts (GET)
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'GET'
})
.then(response => response.json())
.then(data => {
	console.log(data)
}) 
//.then method captures the response object and returns another "promise" which will eventually be "resolved" or "rejected"

// "async" and "await" keywords is another approach that can be used to achieve asynchronous code.
// used in functions to indicate which portions of code should be waited for.

async function fetchData() {

	let result = await fetch('https://jsonplaceholder.typicode.com/posts');

	// Result returned by fetch
	console.log(result);
	console.log(typeof result);
	console.log(result.body);

	// Converts data from the response ojbect as JSON
	let json = await result.json()
	console.log(json);

}
fetchData();



// Creating a post
// create a new post following the REST API(create, POST(method))
fetch('https://jsonplaceholder.typicode.com/posts', {
	method: 'POST',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		title: 'New post',
		body: 'Hello World',
		userId:1
	})
})
.then(response => response.json())
.then(json => console.log(json))


// Updating a post
// update a specific post following the REST API (update, PUT(method))
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method:'PUT',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		id:1,
		title:"Updated post",
		body:"Hello again",
		userId:1
	})
})
.then(res =>res.json())
.then(data => console.log(data))

// The PUT is a method modifying resources where the clinet sends data the updates the ENTIRE resources. It is used to set an entity's information completely.
// The PATCH method applies a partial update to the resources

// Patch method
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method:'PATCH',
	headers: {
		'Content-Type':'application/json'
	},
	body: JSON.stringify({
		id:1,
		title:"Updated post 2",
		// body:"Hello again",
		// userId:1
	})
})
.then(res =>res.json())
.then(data => console.log(data))


// Delete a post
fetch('https://jsonplaceholder.typicode.com/posts/1', {
	method: 'DELETE'
})
.then(res => res.json())
.then(data => console.log(data))


// Filtering post
// The data can be filtered by sending the userId along with the URL
// ?
// Individual Parameters
	// 'url?parameterName=value'
// Multiple Paramters
	// 'url?parameterNameA=valueA&parameterB=valueB'


fetch('https://jsonplaceholder.typicode.com/posts?userId=1')
.then(res => res.json())
.then(data => console.log(data))



// Retrieving nested/related data
fetch('https://jsonplaceholder.typicode.com/posts/1/comments')
.then(res => res.json())
.then(data => console.log(data))